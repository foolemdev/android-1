package com.example.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sum = findViewById<Button>(R.id.sum)
        val sub = findViewById<Button>(R.id.sub)
        val mul = findViewById<Button>(R.id.mul)
        val div = findViewById<Button>(R.id.div)

        sum.setOnClickListener {
            val input1 = Integer.parseInt(findViewById<EditText>(R.id.input1).text.toString())
            val input2 = Integer.parseInt(findViewById<EditText>(R.id.input2).text.toString())
            val res = input1 + input2
            Toast.makeText(this, res.toString(), Toast.LENGTH_SHORT).show()
        }

        sub.setOnClickListener {
            val input1 = Integer.parseInt(findViewById<EditText>(R.id.input1).text.toString())
            val input2 = Integer.parseInt(findViewById<EditText>(R.id.input2).text.toString())
            val res = input1 - input2
            Toast.makeText(this, res.toString(), Toast.LENGTH_SHORT).show()
        }

        mul.setOnClickListener {
            val input1 = Integer.parseInt(findViewById<EditText>(R.id.input1).text.toString())
            val input2 = Integer.parseInt(findViewById<EditText>(R.id.input2).text.toString())
            val res = input1 * input2
            Toast.makeText(this, res.toString(), Toast.LENGTH_SHORT).show()
        }

        div.setOnClickListener {
            val input1 = Integer.parseInt(findViewById<EditText>(R.id.input1).text.toString())
            val input2 = Integer.parseInt(findViewById<EditText>(R.id.input2).text.toString())
            val res = input1 / input2
            Toast.makeText(this, res.toString(), Toast.LENGTH_SHORT).show()
        }

    }
}
